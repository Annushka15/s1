#ifndef MEMORY_H
#define MEMORY_H
#include<memory>
#include <new>

namespace ann {
	template<class T1, class T2>
	void construct(T1* p, const T2& value);
	template<class T>
	void destroy(T* p);
	template <class FwdIter>
	void destroy(FwdIter first, FwdIter last);
	template<typename T>
	class Memory
	{
	protected:
		Memory(size_t size = 0);
		~Memory();
		void Swap(Memory& rhs) throw();
		T* arr_;
		size_t capacity_;
		size_t arrsize_;
	private:
		Memory(const Memory&);
		Memory& operator=(const Memory&);
	};
}

template<class T>
void destroy(T* p)
{
	p->~T();
}
template <class FwdIter>
void destroy(FwdIter first, FwdIter last)
{
	while (first != last)
	{
		destroy(&*first);
		++first;
	}
}

template<class T1, class T2>
void ann::construct(T1* p, const T2& value)
{
	new(p) T1(value);
}

template<typename T>
ann::Memory<T>::Memory(size_t size)
	:arr_(static_cast<T*> (size == 0 ? 0 : operator new(sizeof(T) * size))),
	capacity_(size), arrsize_(0)
{
}

template<typename T>
ann::Memory<T>::~Memory()
{
	destroy(arr_, arr_ + arrsize_);
	delete arr_;
}

template<typename T>
void ann::Memory<T>::Swap(Memory<T>& rhs) throw()
{
	std::swap(arr_, rhs.arr_);
	std::swap(capacity_, rhs.capacity_);
	std::swap(arrsize_, rhs.arrsize_);
}

#endif