#ifndef QUEUE_H
#define QUEUE_H
#include<stdexcept>
#include"Memory.h"

namespace ann {
	template <typename T>
	class Queue : private Memory<T>
	{
	public:
		Queue(size_t size = 0);
		~Queue() = default;
		Queue(const Queue&);
		bool empty();
		void push(const T&);
		size_t count() const;
		const T& front();
		void pop();

	private:
		T* arr_;
		size_t capacity_;
		size_t arrsize_;
	};
}

template<typename T>
ann::Queue<T>::Queue(size_t size)
	:Memory<T>(size)
{
}

template<typename T>
ann::Queue<T>::Queue(const Queue<T>& rhs)
	: Memory<T>(rhs.arrsize_)
{
	while (arrsize_ < rhs.arrsize_)
	{
		ann::construct(arr_ + arrsize_, rhs.arr_[arrsize_]);
		++arrsize_;
	}
}

template<typename T>
bool ann::Queue<T>::empty()
{
	return arrsize_ == 0;
}

template<typename T>
size_t ann::Queue<T>::count() const
{
	return arrsize_;
}

template<typename T>
void ann::Queue<T>::push(const T& rhs)
{
	if (arrsize_ == capacity_)
	{
		Queue<T> temp(capacity_ * 2 + 1);
		while (temp.count() < arrsize_)
		{
			temp.push(arr_[temp.count()]);
		}
		temp.push(rhs);
		this->Swap(temp);
	}
	else
	{
		ann::construct(arr_ + arrsize_, rhs);
		++arrsize_;
	}
}

template<typename T>
const T& ann::Queue<T>::front()
{
	if (empty())
	{
		throw std::logic_error("Empty!");
	}
	return arr_[0];
}

template<typename T>
void ann::Queue<T>::pop()
{
	if (empty())
	{
		throw std::logic_error("Empty!");
	}
	++arrsize_;
	Queue<T> temp(capacity_);
	while (temp.count() < arrsize_ - 1)
	{
		temp.push(arr_[temp.count() + 1]);
	}
	this->Swap(temp);
}

#endif
