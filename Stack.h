#ifndef STACK_H
#define STACK_H
#include<stdexcept>
#include"Memory.h"

namespace ann {
	template <typename T>
	class Stack : private Memory<T>
	{
	public:
		Stack(size_t size = 0);
		~Stack() = default;
		Stack(const Stack&);
		Stack& operator=(const Stack&);
		size_t count() const;
		void push(const T&);
		void pop();
		const T& top();
		bool empty();

	private:
		T* arr_;
		size_t capacity_;
		size_t arrsize_;
	};
}


template <typename T>
ann::Stack<T>::Stack(size_t size)
	:Memory<T>(size)
{
}

template <typename T>
ann::Stack<T>::Stack(const Stack<T>& rhs)
	: Memory<T>(rhs.arrsize_)
{
	while (arrsize_ < rhs.arrsize_)
	{
		ann::construct(arr_ + arrsize_, rhs.arr_[arrsize_]);
		++arrsize_;
	}
}

template <typename T>
ann::Stack<T>& ann::Stack<T>::operator = (const Stack<T>& rhs)
{
	Stack<T> temp(rhs);
	this->Swap(temp);
	return *this;
}

template <typename T>
size_t ann::Stack<T>::count() const
{
	return arrsize_;
}

template <typename T>
void ann::Stack<T>::push(const T& rhs)
{
	if (arrsize_ == capacity_)
	{
		Stack<T> temp(capacity_ * 2 + 1);
		while (temp.count() < arrsize_)
		{
			temp.push(arr_[temp.count()]);
		}
		temp.push(rhs);
		this->Swap(temp);
	}
	else
	{
		ann::construct(arr_ + arrsize_, rhs);
		++arrsize_;
	}
}

template <typename T>
void ann::Stack<T>::pop()
{
	if (empty())
	{
		throw std::logic_error("Empty!");
	}
	else
	{
		--arrsize_;
		destroy(arr_ + arrsize_);
	}
}

template <typename T>
const T& ann::Stack<T>::top()
{
	if (empty())
	{
		throw std::logic_error("Empty!");
	}
	else
	{
		return arr_[arrsize_ - 1];
	}
}

template <typename T>
bool ann::Stack<T>::empty()
{
	return arrsize_ == 0;
}

#endif