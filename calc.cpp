#include<stdexcept>
#include<cctype>
#include<string>
#include"Stack.h"
#include "Queue.h"
#include"matem.h"
#include"calc.h"

void mtm::makePostfix(std::string& lhs, ann::Queue<char>& rhs)
{
	ann::Stack<char> operation;
	int open_bracket = 0;

	if (lhs[0] != '(' || isdigit(lhs[0]))
	{
		throw std::logic_error("Incorrect algebraic expression\n");
	}

	for (size_t i = 0; i < lhs.length(); i++)
	{
		if (lhs[i] == ' ')
		{
			continue;
		}
		else if (isalpha(lhs[i]))
		{
			throw std::logic_error("�annot contain letters\n");
		}
		else if (lhs[i] == '(')
		{
			operation.push(lhs[i]);
			++open_bracket;
		}
		else if (lhs[i] == ')')
		{
			if (open_bracket == 0)
			{
				throw std::logic_error("Incorrect algebraic expression");
			}
			while (operation.top() != '(')
			{
				char temp = operation.top();
				rhs.push(temp);
				operation.pop();
			}
			open_bracket--;
		}
		else if (isdigit(lhs[i]))
		{
			int num = 0;
			while (isdigit(lhs[i])) {
				num *= 10 + (int)(lhs[i]);
				i++;
			}
			i--;
			rhs.push(num);
		}
		else if (mtm::isOperator(lhs[i]))
		{
			if (operation.empty())
			{
				operation.push(lhs[i]);
			}
			else
			{
				if (mtm::prioritet(lhs[i]) < mtm::prioritet(operation.top()))
				{
					operation.push(lhs[i]);
				}
				else
				{
					while (mtm::prioritet(lhs[i]) > mtm::prioritet(operation.top())
						|| mtm::prioritet(lhs[i]) == mtm::prioritet(operation.top()))
					{
						char temp = operation.top();
						rhs.push(temp);
						operation.pop();
					}
					operation.push(lhs[i]);
				}
			}

		}

	}

}

long long mtm::calculateExpression(ann::Queue<char>& rhs)
{
	ann::Stack<long long> calc;

	for (size_t i = 0; i < rhs.count(); i++)
	{
		if (std::isdigit(rhs.front()))
		{
			int temp = (long long)rhs.front();
			rhs.pop();
			calc.push(temp);
		}
		else
		{
			long long a = calc.top();
			calc.pop();
			long long b = calc.top();
			calc.pop();

			switch (rhs.front())
			{
			case '+':
				calc.push(mtm::sum(a, b));
				rhs.pop();
				break;
			case '-':
				calc.push(b - a);
				rhs.pop();
				break;
			case '*':
				calc.push(mtm::multiply(a, b));
				rhs.pop();
				break;
			case '/':
				if (a == 0)
				{
					throw std::logic_error("can't divide by zero");
				}
				else
				{
					calc.push(b / a);
					rhs.pop();
					break;
				}
			case '%':
				if (b < 0)
				{
					calc.push(a + b * (std::abs(a) / b + 1));
					rhs.pop();
					break;
				}
				else
				{
					calc.push(b % a);
					rhs.pop();
					break;
				}
			}
		}
	}
	if (calc.empty())
	{
		throw std::logic_error("no response");
	}
	long long result = calc.top();
	calc.pop();
	return result;
}