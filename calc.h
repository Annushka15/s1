#ifndef POSTFIX_H
#define POSTFIX_H
#include "Queue.h"
#include<string>

namespace mtm 
{
	void makePostfix(std::string& lhs, ann::Queue<char>& rhs);
	long long calculateExpression(ann::Queue<char>& rhs);
}
#endif
