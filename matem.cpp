#include"matem.h"
#include <algorithm> 
#include<stdexcept>

long long mtm::sum(long long a, long long b)
{
	const long long max_ll = std::numeric_limits<long long>::max();
	const long long min_ll = std::numeric_limits<long long>::min();

	if (a > 0 && b > 0)
	{
		if (max_ll - a < b) {
			throw std::overflow_error("Overflow!");
		}
	}
	if (a < 0 && b < 0)
	{
		if (min_ll - a > b)
		{
			throw std::overflow_error("Overflow!");
		}
	}
	return a + b;
}

long long mtm::multiply(long long a, long long b)
{
	const long long max_ll = std::numeric_limits<long long>::max();
	const long long min_ll = std::numeric_limits<long long>::min();

	if ((a > 0 && b > 0) || (a < 0 && b < 0))
	{
		if (max_ll / a < b) {
			throw std::overflow_error("Overflow!");
		}
	}
	if ((a > 0 && b < 0) || (a < 0 && b > 0))
	{
		if (min_ll / a > b)
		{
			throw std::overflow_error("Overflow!");
		}
	}
	return a * b;
}

int mtm::prioritet(const char a)
{
	switch (a)
	{
	case '(': return 1;
	case '/':case'%':case'*':return 2;
	case '+':case'-':return 3;
	}
}

bool mtm::isOperator(char a)
{
	return a == '+' || a == '-'
		|| a == '*' || a == '%'
		|| a == '/';
}