#ifndef MATEM_H
#define MATEM_H

namespace mtm {
	long long sum(long long a, long long b);
	long long multiply(long long a, long long b);
	int prioritet(const char a);
	bool isOperator(char a);
}
#endif
